const getSum = (str1, str2) => 
{

  if(typeof(str1)!='string'||typeof(str2)!='string')
   {
     return false;
     }

  if(str1.length!=str2.length)
    {
      let lenDif= Math.abs(str1.length-str2.length);
      let strZer = '0'.repeat(lenDif);
      if(str1.length>str2.length)
        str2=strZer+str2;
      else  
      {
        str1=strZer+str1;
        }
    }

    let result='';
    let transfer=0;
   for(let i=str1.length-1;i>=0;i--)
   {
    let number1=Number(str1[i]);
    let number2=Number(str2[i]);
    if(isNaN(number1) || isNaN(number2))
    {
      return false;
    }
    let temp=number1+number2+transfer;
    if(temp>9)
    {
      temp-=10;
      transfer=1;
    }
    else 
    {
      transfer=0;
    }

    result = temp.toString()+result;
   }
   if(transfer==1)
   {
     result = '1'+result;
   }
  
  return result;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
let count=0;
let countCom=0;
  for(let item of listOfPosts)
  {
  if(item.author==authorName)
      count++;
      if(item.comments!=undefined)
     {
       for(it of item.comments)
       {
         if(it.author==authorName)
         {
           countCom++;
         }
       }
     }
  }  
  return  `Post:${count},comments:${countCom}`;
};

const tickets=(people)=> {
 let ar25=[];
 let ar50=[];
 let ar100=[];
 for(let item of people)
 {
  switch (item)
  {
    case 25: ar25.push(item);
    break;
    case 50: if(ar25.length>0)
      { 
        ar50.push(item);
        ar25.pop();
      }
      else 
      {
        return "NO";
      }
    break;
    case 100: if(ar25.length>0 && ar50.length>0)
    {
      ar100.push(item);
      ar25.pop();
      ar50.pop();
    }
    else if(ar25.length>2)
    {
      ar100.push(item);
      ar25.pop();
      ar25.pop();
      ar25.pop();
    }
    else 
    {
      return "NO";
    }
  }
 }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
